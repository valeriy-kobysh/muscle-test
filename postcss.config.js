const join = require('path').join
const tailwindJS = join(__dirname, 'tailwind.js')

module.exports = {
  plugins: [
    require('postcss-apply'),
    require('postcss-nested'),
    require('postcss-cssnext')(),
    require('postcss-hexrgba'),
    require('autoprefixer'),
    require('tailwindcss')(tailwindJS),
  ]
}
