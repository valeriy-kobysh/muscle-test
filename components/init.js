import Main from '~/components/Main.vue'
import Section from '~/components/Section.vue'
import Header from '~/components/Header.vue'
import Footer from '~/components/Footer.vue'

import Button from '~/components/Button.vue'
import Heading from '~/components/Heading.vue'

import Recomend from '~/components/Recomend.vue'
import Price from '~/components/Price.vue'

const components = [ Main, Section, Header, Footer, Button, Heading, Recomend, Price ]

export default {
  install(Vue, options) {
    components.forEach(component => {
      Vue.component(component.name, component)
    })

    Vue.component('heading', {
      props: {
        level: {
          type: Number,
          required: true,
        },
        theme: {
          type: String,
          default: 'dark',
        },
      },
      render(createElement) {
        return createElement(
          'h' + this.level, // имя тега
          {
            class: {
              'text-5xl font-heading font-bold uppercase text-center text-dark': this.level == 1,
              'text-3xl font-medium align-middle': this.level == 2,
              'lg:text-2xl text-xl font-medium align-middle': this.level == 3,
              'text-grey-darker': this.theme == 'dark',
              'text-grey-lighter': this.theme == 'light',
            },
          },
          this.$slots.default, // массив дочерних элементов
        )
      },
    })
  },
}
